/*
 * Copyright 2012, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "WifiDisplaySink"
#include <utils/Log.h>

#include "WifiDisplaySink.h"
#include "ParsedMessage.h"
#include "RTPSink.h"

#include <binder/IServiceManager.h>
#include <media/IHDCP.h>
#include <media/IMediaPlayerService.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/MediaErrors.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <cutils/properties.h>

namespace android {

WifiDisplaySink::WifiDisplaySink(
        const sp<ANetworkSession> &netSession)
    : mUsingHDCP(false),
      mHDCPInitializationComplete(false),
      mHDCP(NULL),
      mState(UNDEFINED),
      mNetSession(netSession),
      mReConnectTimes(20),
      mSessionID(0),
      mNextCSeq(1) {
}

WifiDisplaySink::~WifiDisplaySink() {
}

void WifiDisplaySink::start(const char *sourceHost, int32_t sourcePort) {
    sp<AMessage> msg = new AMessage(kWhatStart, id());
	ALOGI("start---------(shost,sport)");
    msg->setString("sourceHost", sourceHost);
    msg->setInt32("sourcePort", sourcePort);
    msg->post();
}

void WifiDisplaySink::start(const char *uri) {
    sp<AMessage> msg = new AMessage(kWhatStart, id());
	ALOGI("start---------(uri)");
    msg->setString("setupURI", uri);
    msg->post();
}

// static
bool WifiDisplaySink::ParseURL(
        const char *url, AString *host, int32_t *port, AString *path,
        AString *user, AString *pass) {
    host->clear();
    *port = 0;
    path->clear();
    user->clear();
    pass->clear();

    if (strncasecmp("rtsp://", url, 7)) {
        return false;
    }

    const char *slashPos = strchr(&url[7], '/');

    if (slashPos == NULL) {
        host->setTo(&url[7]);
        path->setTo("/");
    } else {
        host->setTo(&url[7], slashPos - &url[7]);
        path->setTo(slashPos);
    }

    ssize_t atPos = host->find("@");

    if (atPos >= 0) {
        // Split of user:pass@ from hostname.

        AString userPass(*host, 0, atPos);
        host->erase(0, atPos + 1);

        ssize_t colonPos = userPass.find(":");

        if (colonPos < 0) {
            *user = userPass;
        } else {
            user->setTo(userPass, 0, colonPos);
            pass->setTo(userPass, colonPos + 1, userPass.size() - colonPos - 1);
        }
    }

    const char *colonPos = strchr(host->c_str(), ':');

    if (colonPos != NULL) {
        char *end;
        unsigned long x = strtoul(colonPos + 1, &end, 10);

        if (end == colonPos + 1 || *end != '\0' || x >= 65536) {
            return false;
        }

        *port = x;

        size_t colonOffset = colonPos - host->c_str();
        size_t trailing = host->size() - colonOffset;
        host->erase(colonOffset, trailing);
    } else {
        *port = 554;
    }

    return true;
}

void WifiDisplaySink::onMessageReceived(const sp<AMessage> &msg) {
    switch (msg->what()) {
        case kWhatStart:
        {
            //int32_t sourcePort;

	    ALOGI("start   sink");
            if (msg->findString("setupURI", &mSetupURI)) {
                AString path, user, pass;
                CHECK(ParseURL(
                            mSetupURI.c_str(),
                            &mRTSPHost, &mRTSPPort, &path, &user, &pass)
                        && user.empty() && pass.empty());
            } else {
                CHECK(msg->findString("sourceHost", &mRTSPHost));
                CHECK(msg->findInt32("sourcePort", &mRTSPPort));
            }

            sp<AMessage> notify = new AMessage(kWhatRTSPNotify, id());

            status_t err = mNetSession->createRTSPClient(
                    mRTSPHost.c_str(), mRTSPPort, notify, &mSessionID);
            CHECK_EQ(err, (status_t)OK);

            mState = CONNECTING;
            break;
        }

        case kWhatRTSPNotify:
        {
            int32_t reason;
            CHECK(msg->findInt32("reason", &reason));

            switch (reason) {
                case ANetworkSession::kWhatError:
                {
                    int32_t sessionID;
                    CHECK(msg->findInt32("sessionID", &sessionID));

                    int32_t err;
                    CHECK(msg->findInt32("err", &err));

                    if ((mState == CONNECTING) && (err == -111)){
                        if (mReConnectTimes--){
                           ALOGD("RTSP server may not yet run, try reconnect");
                           usleep(200*1000);
                           start(mRTSPHost.c_str(),mRTSPPort);
                           break;
                        }else{
                           ALOGD("times out connecting to RTSP server, quit!"); 
                           property_set("wfd.sink.status", "ERROR");
                           break;
                        };
                    }

                    AString detail;
                    CHECK(msg->findString("detail", &detail));

                    ALOGE("An error occurred in session %d (%d, '%s/%s').",
                          sessionID,
                          err,
                          detail.c_str(),
                          strerror(-err));

                    if (sessionID == mSessionID) {
                        ALOGI("Lost control connection.");
                        // The control connection is dead now.
                        mNetSession->destroySession(mSessionID);
                        mSessionID = 0;
			finishStop();
                        looper()->stop();
                    }
                    break;
                }

                case ANetworkSession::kWhatConnected:
                {
                    ALOGI("We're now connected.");
                    mState = CONNECTED;
                    property_set("wfd.sink.status", "OK");

                    if (!mSetupURI.empty()) {
                        status_t err =
                            sendDescribe(mSessionID, mSetupURI.c_str());

                        CHECK_EQ(err, (status_t)OK);
                    }
                    break;
                }

                case ANetworkSession::kWhatData:
                {
                    onReceiveClientData(msg);
                    break;
                }

                case ANetworkSession::kWhatBinaryData:
                {
                    CHECK(sUseTCPInterleaving);

                    int32_t channel;
                    CHECK(msg->findInt32("channel", &channel));

                    sp<ABuffer> data;
                    CHECK(msg->findBuffer("data", &data));

                    mRTPSink->injectPacket(channel == 0 /* isRTP */, data);
                    break;
                }

                default:
                    TRESPASS();
            }
            break;
        }

		 case kWhatHDCPNotify:
        {
            int32_t msgCode, ext1, ext2;
            CHECK(msg->findInt32("msg", &msgCode));
            CHECK(msg->findInt32("ext1", &ext1));
            CHECK(msg->findInt32("ext2", &ext2));

            ALOGI("Saw HDCP notification code %d, ext1 %d, ext2 %d",
                    msgCode, ext1, ext2);

            switch (msgCode) {
                case HDCPModule::HDCP_INITIALIZATION_COMPLETE:
                {
                    mHDCPInitializationComplete = true;
                    break;
                }
				
				case HDCPModule::HDCP_INITIALIZATION_FAILED:
				{
					mHDCPInitializationComplete = false;
					break;
				}

                case HDCPModule::HDCP_SHUTDOWN_COMPLETE:
                case HDCPModule::HDCP_SHUTDOWN_FAILED:
                {
                    // Ugly hack to make sure that the call to
                    // HDCPObserver::notify is completely handled before
                    // we clear the HDCP instance and unload the shared
                    // library :(
                   finishStop2();
                    break;
                }

                default:
                {
                    ALOGE("HDCP failure, shutting down.");
                    break;
                }
            }
            break;
        }

        case kWhatStop:
        {
            ALOGD("stop wifidisplay sink session");
	    mNetSession->destroySession(mSessionID);
            mSessionID = 0;
            looper()->stop();
            break;
        }

        default:
            TRESPASS();
    }
}

void WifiDisplaySink::registerResponseHandler(
        int32_t sessionID, int32_t cseq, HandleRTSPResponseFunc func) {
    ResponseID id;
    id.mSessionID = sessionID;
    id.mCSeq = cseq;
    mResponseHandlers.add(id, func);
}

status_t WifiDisplaySink::sendM2(int32_t sessionID) {
    AString request = "OPTIONS * RTSP/1.0\r\n";
    AppendCommonResponse(&request, mNextCSeq);

    request.append(
            "Require: org.wfa.wfd1.0\r\n"
            "\r\n");

	ALOGI("*******************************");
	ALOGI("%s\n",request.c_str());
	ALOGI("*******************************");
    status_t err =
        mNetSession->sendRequest(sessionID, request.c_str(), request.size());

    if (err != OK) {
        return err;
    }

    registerResponseHandler(
            sessionID, mNextCSeq, &WifiDisplaySink::onReceiveM2Response);

    ++mNextCSeq;

    return OK;
}

status_t WifiDisplaySink::onReceiveM2Response(
        int32_t sessionID, const sp<ParsedMessage> &msg) {
    int32_t statusCode;
    if (!msg->getStatusCode(&statusCode)) {
        return ERROR_MALFORMED;
    }

    if (statusCode != 200) {
        return ERROR_UNSUPPORTED;
    }

    return OK;
}

status_t WifiDisplaySink::onReceiveDescribeResponse(
        int32_t sessionID, const sp<ParsedMessage> &msg) {
    int32_t statusCode;
    if (!msg->getStatusCode(&statusCode)) {
        return ERROR_MALFORMED;
    }

    if (statusCode != 200) {
        return ERROR_UNSUPPORTED;
    }

    return sendSetup(sessionID, mSetupURI.c_str());
}

status_t WifiDisplaySink::onReceiveSetupResponse(
        int32_t sessionID, const sp<ParsedMessage> &msg) {
    int32_t statusCode;
	ALOGI("onReceiveSetupResponse********************");
    if (!msg->getStatusCode(&statusCode)) {
		ALOGI("status error******************");
        return ERROR_MALFORMED;
    }

    if (statusCode != 200) {
        return ERROR_UNSUPPORTED;
    }

    if (!msg->findString("session", &mPlaybackSessionID)) {
        return ERROR_MALFORMED;
    }

    if (!ParsedMessage::GetInt32Attribute(
                mPlaybackSessionID.c_str(),
                "timeout",
                &mPlaybackSessionTimeoutSecs)) {
        mPlaybackSessionTimeoutSecs = -1;
    }

    ssize_t colonPos = mPlaybackSessionID.find(";");
    if (colonPos >= 0) {
        // Strip any options from the returned session id.
        mPlaybackSessionID.erase(
                colonPos, mPlaybackSessionID.size() - colonPos);
    }
    
    status_t err = configureTransport(msg);

    if (err != OK) {
        return err;
    }

    mState = PAUSED;

    return sendPlay(
            sessionID,
            !mSetupURI.empty()
                ? mSetupURI.c_str() : mUrl.c_str());
}

status_t WifiDisplaySink::configureTransport(const sp<ParsedMessage> &msg) {
    if (sUseTCPInterleaving) {
        return OK;
    }

    AString transport;
    if (!msg->findString("transport", &transport)) {
        ALOGE("Missing 'transport' field in SETUP response.");
        return ERROR_MALFORMED;
    }

    AString sourceHost;
    if (!ParsedMessage::GetAttribute(
                transport.c_str(), "source", &sourceHost)) {
        sourceHost = mRTSPHost;
    }

    AString serverPortStr;

     /* mtk phone don't response server_port field, but nexus4 do. so 
       deal with seperately. In fact, wifi-spec requires this field, 
       we can use udp connct here as like tcp if server_port is known */
    if (!ParsedMessage::GetAttribute(
                transport.c_str(), "server_port", &serverPortStr)) {
        ALOGE("Missing 'server_port' in Transport field.");
        //return ERROR_MALFORMED;
        return OK;
    } else {

        int rtpPort, rtcpPort;
        ALOGI("serverPortStr = %s\n",serverPortStr.c_str());
        if(serverPortStr.find("-") >= 0)
        {
                if (sscanf(serverPortStr.c_str(), "%d-%d", &rtpPort, &rtcpPort) != 2
                || rtpPort <= 0 || rtpPort > 65535
                || rtcpPort <=0 || rtcpPort > 65535
                ) {
                        ALOGE("%d",rtcpPort);
                        ALOGE("Invalid server_port description '%s'.",
                            serverPortStr.c_str());

                        return ERROR_MALFORMED;
                }
        }
        else
        {
                if(sscanf(serverPortStr.c_str(), "%d", &rtpPort) == 1)
                {
                        rtcpPort = rtpPort+1;
                }
        }

        if (rtpPort & 1) {
           ALOGW("Server picked an odd numbered RTP port.");
        }

        //return mRTPSink->connect(sourceHost.c_str(), rtpPort, rtcpPort);
        return OK;
   }
}


status_t WifiDisplaySink::onReceiveTearDownResponse(
         int32_t sessionID, const sp<ParsedMessage> &msg){
       int32_t statusCode;
    if (!msg->getStatusCode(&statusCode)) {
        return ERROR_MALFORMED;
    }

    ALOGI("received teardown response status code is %d", statusCode);

    if (statusCode != 200) {
        return ERROR_UNSUPPORTED;
    }
    
    mState = TEARDOWN;
    return OK; 
 
}


status_t WifiDisplaySink::onReceivePlayResponse(
        int32_t sessionID, const sp<ParsedMessage> &msg) {
    int32_t statusCode;
    if (!msg->getStatusCode(&statusCode)) {
        return ERROR_MALFORMED;
    }

    if (statusCode != 200) {
        return ERROR_UNSUPPORTED;
    }

    mState = PLAYING;

    return OK;
}

void WifiDisplaySink::onReceiveClientData(const sp<AMessage> &msg) {
    int32_t sessionID;
    CHECK(msg->findInt32("sessionID", &sessionID));

    sp<RefBase> obj;
    CHECK(msg->findObject("data", &obj));

    sp<ParsedMessage> data =
        static_cast<ParsedMessage *>(obj.get());

    //ALOGD("session %d received %s",sessionID, data->debugString().c_str());
	ALOGI("****************************************\n");
	ALOGI("%s\n",data->debugString().c_str());
	ALOGI("****************************************\n");

    AString method;
    AString uri;
    data->getRequestField(0, &method);

    int32_t cseq;
    if (!data->findInt32("cseq", &cseq)) {
        sendErrorResponse(sessionID, "400 Bad Request", -1 /* cseq */);
        return;
    }

    if (method.startsWith("RTSP/")) {
        // This is a response.

        ResponseID id;
        id.mSessionID = sessionID;
        id.mCSeq = cseq;

        ssize_t index = mResponseHandlers.indexOfKey(id);

        if (index < 0) {
            ALOGW("Received unsolicited server response, cseq %d", cseq);
            return;
        }

        HandleRTSPResponseFunc func = mResponseHandlers.valueAt(index);
        mResponseHandlers.removeItemsAt(index);

        status_t err = (this->*func)(sessionID, data);
        CHECK_EQ(err, (status_t)OK);
    } else {
        AString version;
        data->getRequestField(2, &version);
        if (!(version == AString("RTSP/1.0"))) {
            sendErrorResponse(sessionID, "505 RTSP Version not supported", cseq);
            return;
        }

        if (method == "OPTIONS") {
            onOptionsRequest(sessionID, cseq, data);
        } else if (method == "GET_PARAMETER") {
            onGetParameterRequest(sessionID, cseq, data);
        } else if (method == "SET_PARAMETER") {
            onSetParameterRequest(sessionID, cseq, data);
        } else {
            sendErrorResponse(sessionID, "405 Method Not Allowed", cseq);
			ALOGD("onReceiveClientData::----- error 405");
        }
    }
}

void WifiDisplaySink::onOptionsRequest(
        int32_t sessionID,
        int32_t cseq,
        const sp<ParsedMessage> &data) {
    AString response = "RTSP/1.0 200 OK\r\n";
    AppendCommonResponse(&response, cseq);
    response.append("Public: org.wfa.wfd1.0, GET_PARAMETER, SET_PARAMETER\r\n");
    response.append("\r\n");

    status_t err = mNetSession->sendRequest(sessionID, response.c_str());
    CHECK_EQ(err, (status_t)OK);

    err = sendM2(sessionID);
    CHECK_EQ(err, (status_t)OK);
}

void WifiDisplaySink::onGetParameterRequest(
        int32_t sessionID,
        int32_t cseq,
        const sp<ParsedMessage> &data) {
    AString msession;
	bool find = data->findString("Session",&msession);
    if(!find)
    {
            const char *request_param = data->getContent();
	    AString body;  
            char prop_value[PROPERTY_VALUE_MAX] = {'\0'};
            /* answers capabilities that the WFD source are only interested in */
            if (strstr(request_param, "wfd_video_formats"))
                body.append("wfd_video_formats: 40 00 02 02 0001DEFF 157C7FFF 00000FFF 00 0000 0000 11 none none, 01 02 0001DEFF 157C7FFF 00000FFF 00 0000 0000 11 none none\r\n");
            if (strstr(request_param, "wfd_audio_codecs"))
               body.append("wfd_audio_codecs: LPCM 00000003 00, AAC 0000000F 00\r\n");

            /* answers capabilities that the WFD source are only interested in */
            if (strstr(request_param, "wfd_3d_video_formats"))
                body.append("wfd_3d_video_formats: none\r\n");

            if (strstr(request_param, "wfd_uibc_capability"))
                body.append("wfd_uibc_capability: none\r\n");

            /* using HDCP only if HDCP2.x Key inside, otherwise not support*/
            if (strstr(request_param, "wfd_content_protection")) {
                 if (property_get("ro.secure.hdcp2xkey.inside",prop_value,NULL) && !strcmp(prop_value,"true")){
                      mUsingHDCP = true;
                      body.append(StringPrintf("wfd_content_protection: HDCP2.1 port=%d\r\n",kHDCPDefaultPort));
                 }
                 else {
                      body.append(StringPrintf("wfd_content_protection: none\r\n"));
                      mUsingHDCP = false;
                 }
                 ALOGD("key inside ?:%s",prop_value);
            } else {
                mUsingHDCP = false;
            }

            if (strstr(request_param, "wfd_display_edid"))
               body.append("wfd_display_edid: none\r\n");

            if (strstr(request_param, "wfd_coupled_sink"))
               body.append("wfd_coupled_sink: none\r\n");

            if (strstr(request_param, "wfd_standby_resume_capability"))
               body.append("wfd_standby_resume_capability: supported\r\n");

            if (strstr(request_param, "wfd_client_rtp_ports"))
            	body.append("wfd_client_rtp_ports: RTP/AVP/UDP;unicast 15550 0 mode=play\r\n");

            if (mUsingHDCP) {
                status_t errHDCP = makeHDCP();
                if (errHDCP != OK) {
                   ALOGE("Unable to instantiate HDCP component.");
                   mUsingHDCP = false;
                }
                /* important! sleep 100ms to ensure hdcp listen thread is running up */
                usleep(100*1000);
            }

	    AString response = "RTSP/1.0 200 OK\r\n";
	    AppendCommonResponse(&response, cseq);
	    response.append("Content-Type: text/parameters\r\n");
	    response.append(StringPrintf("Content-Length: %d\r\n", body.size()));
	    response.append("\r\n");
	    response.append(body);
	    ALOGD("send m3 response\n");
	    ALOGD("%s\n",response.c_str());
	    status_t err = mNetSession->sendRequest(sessionID, response.c_str());
	    CHECK_EQ(err, (status_t)OK);
    } else {
	    ALOGD("onReceiveClientData::----- m16");
	    AString response = "RTSP/1.0 200 OK\r\n";
	    AppendCommonResponse(&response, cseq);
	    //response.append(StringPrintf("Session: %s\r\n", mPlaybackSessionID.c_str()));
            response.append("\r\n");
	    status_t err = mNetSession->sendRequest(sessionID, response.c_str());
	    ALOGI("%s\n",response.c_str());
	    CHECK_EQ(err, (status_t)OK);
    }
}

status_t WifiDisplaySink::sendDescribe(int32_t sessionID, const char *uri) {
    uri = "rtsp://xwgntvx.is.livestream-api.com/livestreamiphone/wgntv";
    uri = "rtsp://v2.cache6.c.youtube.com/video.3gp?cid=e101d4bf280055f9&fmt=18";

    AString request = StringPrintf("DESCRIBE %s RTSP/1.0\r\n", uri);
    AppendCommonResponse(&request, mNextCSeq);

    request.append("Accept: application/sdp\r\n");
    request.append("\r\n");

    status_t err = mNetSession->sendRequest(
            sessionID, request.c_str(), request.size());

    if (err != OK) {
        return err;
    }

    registerResponseHandler(
            sessionID, mNextCSeq, &WifiDisplaySink::onReceiveDescribeResponse);

    ++mNextCSeq;

    return OK;
}

status_t WifiDisplaySink::sendSetup(int32_t sessionID, const char *uri) {

    mRTPSink = new RTPSink(mNetSession);

    looper()->registerHandler(mRTPSink);

    status_t err = mRTPSink->init(sUseTCPInterleaving);

    if (err != OK) {
        looper()->unregisterHandler(mRTPSink->id());
        mRTPSink.clear();
        return err;
    }

    AString request = StringPrintf("SETUP %s RTSP/1.0\r\n", uri);

    AppendCommonResponse(&request, mNextCSeq);

    if (sUseTCPInterleaving) {
        request.append("Transport: RTP/AVP/TCP;interleaved=0-1\r\n");
    } else {
        int32_t rtpPort = mRTPSink->getRTPPort();

        request.append(
                StringPrintf(
                    "Transport: RTP/AVP/UDP;unicast;client_port=%d\r\n",
                    rtpPort));
    }

    request.append("\r\n");
    ALOGI("request = '%s'", request.c_str());
    err = mNetSession->sendRequest(sessionID, request.c_str(), request.size());

    if (err != OK) {
        return err;
    }

    registerResponseHandler(
            sessionID, mNextCSeq, &WifiDisplaySink::onReceiveSetupResponse);
    ++mNextCSeq;

    return OK;
}

status_t WifiDisplaySink::sendPlay(int32_t sessionID, const char *uri) {
    AString request = StringPrintf("PLAY %s RTSP/1.0\r\n", uri);

    AppendCommonResponse(&request, mNextCSeq);

    request.append(StringPrintf("Session: %s\r\n", mPlaybackSessionID.c_str()));
    request.append("\r\n");

    status_t err =
        mNetSession->sendRequest(sessionID, request.c_str(), request.size());

    if (err != OK) {
        return err;
    }

    registerResponseHandler(
            sessionID, mNextCSeq, &WifiDisplaySink::onReceivePlayResponse);
    ++mNextCSeq;
    return OK;
}

status_t WifiDisplaySink::sendTearDown(int32_t sessionID, const char *uri) {
    AString request = StringPrintf("TEARDOWN %s RTSP/1.0\r\n", uri);

    AppendCommonResponse(&request, mNextCSeq);

    request.append(StringPrintf("Session: %s\r\n", mPlaybackSessionID.c_str()));
    request.append("\r\n");

    status_t err =
        mNetSession->sendRequest(sessionID, request.c_str(), request.size());
    if (err != OK) {
		finishStop();
        return err;
    }

    registerResponseHandler(
            sessionID, mNextCSeq, &WifiDisplaySink::onReceiveTearDownResponse);
    ++mNextCSeq;
	finishStop();
    return OK;
}


void WifiDisplaySink::sendIDR(int32_t sessionID, const char *uri)
{
    AString request = StringPrintf("SET_PARAMETER %s RTSP/1.0\r\n", uri);

    AppendCommonResponse(&request, mNextCSeq);
    AString body = "wfd_idr_request\r\n";
    request.append("Content-Type: text/parameters\r\n");
    request.append(StringPrintf("Content-Length: %d\r\n", body.size()));
    request.append("\r\n");
    request.append(body);
    
    status_t err =
        mNetSession->sendRequest(sessionID, request.c_str(), request.size());

    ALOGI("%s\n",request.c_str());

    registerResponseHandler(
            sessionID, mNextCSeq, &WifiDisplaySink::onReceiveIdrResponse);
    
    if (err != OK) {
        return;
    }
    
    ++mNextCSeq;
    
}


status_t WifiDisplaySink::onReceiveIdrResponse(
        int32_t sessionID, const sp<ParsedMessage> &msg) {
    int32_t statusCode;
    if (!msg->getStatusCode(&statusCode)) {
        return ERROR_MALFORMED;
    }

    if (statusCode != 200) {
        return ERROR_UNSUPPORTED;
    }
    return OK;
}

void WifiDisplaySink::onSetParameterRequest(
        int32_t sessionID,
        int32_t cseq,
        const sp<ParsedMessage> &data) {
    if (mUsingHDCP && !mHDCPInitializationComplete) {
		ALOGI("HDCP initialization uncompletes.");
	}
    const char *content = data->getContent();
    if (strstr(content, "wfd_trigger_method: SETUP\r\n") != NULL) {
            /* xiaomi2s seems not to support M13(IDR) request, it will redo the process of 
              session negociation, but IDR is necessary, the sink have to send IDR to xiaomi2s.  
              so regard setup trigger as bad request when rtsp session has been established */
            if (mState == PLAYING) {
               ALOGD("rtsp session has been established, it is a bad request!");
               sendErrorResponse(sessionID, "400 Bad Request", cseq);
               return;
            }	

	    AString response = "RTSP/1.0 200 OK\r\n";
	    AppendCommonResponse(&response, cseq);
	    response.append("\r\n");
		ALOGI("send m5 response(), %s\n",response.c_str());
	    status_t err1 = mNetSession->sendRequest(sessionID, response.c_str());
	    CHECK_EQ(err1, (status_t)OK);

        status_t err =
            sendSetup(
                    sessionID,
                    mUrl.c_str());//rtsp://x.x.x.x:x/wfd1.0/streamid=0
            CHECK_EQ(err, (status_t)OK);
    } else if (strstr(content, "wfd_trigger_method: TEARDOWN\r\n") != NULL) {
            ALOGI("receive source trigger teardown request"); 
            AString response = "RTSP/1.0 200 OK\r\n";
            AppendCommonResponse(&response, cseq);
            response.append("\r\n");
            status_t err1 = mNetSession->sendRequest(sessionID, response.c_str());
			if(err1!=OK){
				finishStop();
				return;
			}
            status_t err = sendTearDown(sessionID, mUrl.c_str());//rtsp://x.x.x.x:x/wfd1.0/streamid=0
			if(err!=OK){
				finishStop();
			}
    } else if (strstr(content, "wfd_trigger_method: PLAY\r\n") != NULL) {
            ALOGI("receive source trigger play request"); 
            AString response = "RTSP/1.0 200 OK\r\n";
            AppendCommonResponse(&response, cseq);
            response.append("\r\n");
            status_t err1 = mNetSession->sendRequest(sessionID, response.c_str());
            CHECK_EQ(err1, (status_t)OK); 

            status_t err = sendPlay(sessionID,!mSetupURI.empty()
                ? mSetupURI.c_str() : mUrl.c_str()); 
            CHECK_EQ(err, (status_t)OK);
    } else if (strstr(content, "wfd_presentation_URL:") != NULL) {
	    AString temp(content);
	    int quo = temp.find("rtsp://");
	    if(quo >= 0) {
		int space = temp.find(" ",quo+1);
		if(space >= 0){
                   mUrl = AString(temp,quo,space-quo);
                   ALOGD("wfd_presentation_URL:%s",mUrl.c_str());
                }
	    }else {
                //the source didn't give us the url, we need to create one.
                mUrl = AString(StringPrintf("rtsp://%s/wfd1.0/streamid=0",mRTSPHost.c_str()));
                ALOGD("missing url,create one wfd-presentation-url:%s",mUrl.c_str());
            }

		char* pos2;
		if((pos2 = strstr(content,"AAC")) != NULL){
				//AAC codec
			}else if ((pos2 = strstr(content,"LPCM")) != NULL){
				//LPCM codec
			}else{
				//LPCM
		}
		
		AString response = "RTSP/1.0 200 OK\r\n";
	    AppendCommonResponse(&response, cseq);
	    response.append("\r\n");
		ALOGI("send m4 response\n");
		ALOGI("%s\n",response.c_str());
	        status_t err = mNetSession->sendRequest(sessionID, response.c_str());
	        CHECK_EQ(err, (status_t)OK);
     } else if (strstr(content, "wfd_standby")) {
            /* the sender is entering standby mode */
            ALOGD("onSetParmeterRequest content %s",content);
            AString response = "RTSP/1.0 200 OK\r\n";
            AppendCommonResponse(&response, cseq);
            response.append("\r\n");
            status_t err = mNetSession->sendRequest(sessionID, response.c_str());
            CHECK_EQ(err, (status_t)OK);
     } else {
	    //for furture use 
	    ALOGD("onSetParmeterRequest content %s",content);
            AString response = "RTSP/1.0 200 OK\r\n";
            AppendCommonResponse(&response, cseq);
            response.append("\r\n");
            status_t err = mNetSession->sendRequest(sessionID, response.c_str());
            CHECK_EQ(err, (status_t)OK);
     }
}

void WifiDisplaySink::sendErrorResponse(
        int32_t sessionID,
        const char *errorDetail,
        int32_t cseq) {
    AString response;
    response.append("RTSP/1.0 ");
    response.append(errorDetail);
    response.append("\r\n");

    AppendCommonResponse(&response, cseq);

    response.append("\r\n");

    status_t err = mNetSession->sendRequest(sessionID, response.c_str());
    CHECK_EQ(err, (status_t)OK);
}

void WifiDisplaySink::finishStop() {
    if (mHDCP != NULL&&mHDCPInitializationComplete) {
        ALOGI("**Initiating HDCP shutdown.");
        mHDCP->shutdownAsync();
        mHDCP->setObserver(NULL);
        mHDCPObserver.clear();
        mHDCP.clear();
        mHDCP = NULL;
        //return;
    }
    
    (new AMessage(kWhatStop, id()))->post(30000l);
}

void WifiDisplaySink::finishStop2() {
	if (mHDCP != NULL) {
        ALOGI("finishStop2.");
	mHDCP->setObserver(NULL);
        mHDCPObserver.clear();
        mHDCP.clear();
        mHDCP = NULL;
    }
	(new AMessage(kWhatStop, id()))->post(30000l);
}


struct WifiDisplaySink::HDCPObserver : public BnHDCPObserver {
    HDCPObserver(const sp<AMessage> &notify);

    virtual void notify(
            int msg, int ext1, int ext2, const Parcel *obj);

private:
    sp<AMessage> mNotify;

    DISALLOW_EVIL_CONSTRUCTORS(HDCPObserver);
};

WifiDisplaySink::HDCPObserver::HDCPObserver(
        const sp<AMessage> &notify)
    : mNotify(notify) {
}

void WifiDisplaySink::HDCPObserver::notify(
        int msg, int ext1, int ext2, const Parcel *obj) {
    sp<AMessage> notify = mNotify->dup();
    notify->setInt32("msg", msg);
    notify->setInt32("ext1", ext1);
    notify->setInt32("ext2", ext2);
    notify->post();
}

status_t WifiDisplaySink::makeHDCP() {
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.player"));
    sp<IMediaPlayerService> service = interface_cast<IMediaPlayerService>(binder);
    CHECK(service != NULL);
    mHDCP = service->makeHDCP(false);
    if (mHDCP == NULL) {
        return ERROR_UNSUPPORTED;
    }

    sp<AMessage> notify = new AMessage(kWhatHDCPNotify, id());
    mHDCPObserver = new HDCPObserver(notify);
    status_t err = mHDCP->setObserver(mHDCPObserver);
    if (err != OK) {
        ALOGE("Failed to set HDCP observer.");
        mHDCPObserver.clear();
        mHDCP.clear();
        return err;
    }

	char* localIP = getFirstLocalAddress();
    err = mHDCP->initAsync(localIP, kHDCPDefaultPort);
    if (err != OK) {
        return err;
    }
    return OK;
}

char* WifiDisplaySink::getFirstLocalAddress(){
	char local[255] = {0};
	gethostname(local, sizeof(local));
	hostent* ph = gethostbyname(local);
	if(ph == NULL)
		return NULL;
	char *addrChar = NULL;
	for(int i=0; ph->h_addr_list[i]; ++i){
		in_addr addr;
		memcpy(&addr, ph->h_addr_list[i], sizeof(in_addr));
		addrChar = (inet_ntoa(addr));
		AString addrStr(addrChar);
		if(addrStr=="127.0.0.1"){
			addrChar = NULL;	
			continue;
		}
	}
	if(addrChar==NULL){
		char* anyAddr = (char*)"0.0.0.0";
		return anyAddr;
	}
	return addrChar;
}



// static
void WifiDisplaySink::AppendCommonResponse(AString *response, int32_t cseq) {
    time_t now = time(NULL);
    struct tm *now2 = gmtime(&now);
    char buf[128];
    strftime(buf, sizeof(buf), "%a, %d %b %Y %H:%M:%S %z", now2);

    response->append("Date: ");
    response->append(buf);
    response->append("\r\n");
    response->append("User-Agent: RK/RK01 stagefright/1.1 (Linux;Android 4.1)\r\n");

    if (cseq >= 0) {
        response->append(StringPrintf("CSeq: %d\r\n", cseq));
    }
}

}  // namespace android
